﻿using UnityEngine;
using System.Collections;

public static class GameVariables
{
    public static bool key;
    public static bool paper_demon_name_geh;
    public static bool paper_demon_name_loh;
    public static bool donut;
    public static bool bow;
    public static int nbDemons = 3;
    public static int nbTargets = 3;
    public static float time;
}
