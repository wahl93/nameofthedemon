# Name of the Demon #

Final Game "Name of the Demon" as a group project for Game Development at TU Ilmenau WS2018/2019.

## What is the game? ##

This game is a VR horror experience. The player has to win mini games to discover the whole story.

## Complete Story ##

One morning a student receives an email from her professor on her computer. He describes a physics experiments, which went wrong. He asks for her help. Further he explains that she will find more information in his office.
The student decides to help the professor and shortly after she is in the dark hallway of the university. Here she finds the office of her professor on the right side.
After entering it, she discovers that it is dark in the office and her battery is running low. So, she has to be quick to search all the objects, which the professor wrote on his board.
She finds a bow, a donut, a piece of paper and the key to the next room. There are also some research papers, which are describing the experiment.
After that, she can go to the professor’s laboratory.
In the lab 3 little demons are guarding a door. When the student enters, the demons attack her.
She fights them with the bow from the professor’s office.
When they all are dead, a larger demon appears, and she only hears the screaming professor. He tells her, that she can destroy the demon by knowing his true name. She also has to throw the ultimate donut.
She does so and a bright light appears. The student has no energy left and blacks out in the lab.
The student awakes in her dorm room and receives another email from her professor, who is thankful for her help and regards her with a donut coupon.

## How to play the game? ##

To play the game you need a HTC Vive System set up into your room. If everything is set up, start NameOfTheDemon.exe.


## Important Folders and Files ##

### Assets ###
* Animation
* Icon
* Scenes
* Scripts
* Soundeffects
* Videos
* WritingFonts

### BlenderObj ###
* Board
* DemonPowerBall
* Donut
* Monster
* Prologe
* Mug
* Piece Of Papers: geh, loh

### NameOfTheDemonGame ###
* NameOfTheDemon.exe (Game)

### VideoPics ###
* Prologe
* Epilogue

### Other Documents ###
* GDDDemon.pdf (Documentation for more information)
* NameOfTheDemonLinkTasks.pdf (Tasks of group members)

## Aufgabenverteilung ##

### Alexandra Draghici ###

* Story
* Sound
* Menus
* Interfaces
* Prolog/Epilog Scenes
* HTC Vive Utility
* Video Edit (Prolog/Epilog)
* Timer in Office
* Game Over Scenes
* Writing Font

### Alexandra Wahl ###

* Story
* Game Design (Hallway, Office, Prof. Lab)
* Blender Objects (Students Dorm Room, Donut, Different Papers, Ultimate Demon, Mug)
* Fighting System (Player and Demons)
* VRTK Interactions with Objects (Pick up Objects, Trigger Door Knob)
* Animation Little Demons
* Tutorials (Hallway, Prof. Lab2)

## Contact ##

Alexandra Wahl: wahlalexandra93@gmail.com
   
    
    
